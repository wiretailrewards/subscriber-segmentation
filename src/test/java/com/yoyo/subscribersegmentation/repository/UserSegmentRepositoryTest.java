//package com.yoyo.subscribersegmentation.repository;
//
//import com.yoyo.subscribersegmentation.domain.models.Segment;
//import com.yoyo.subscribersegmentation.domain.models.Tenant;
//import com.yoyo.subscribersegmentation.domain.models.User;
//import com.yoyo.subscribersegmentation.domain.models.UserSegment;
//import com.yoyo.subscribersegmentation.domain.request.CreateSegmentRequest;
//import com.yoyo.subscribersegmentation.domain.request.CreateTenantRequest;
//import org.junit.Ignore;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
//
//import java.util.Date;
//import java.util.List;
//
//import static org.mockito.ArgumentMatchers.anyString;
//
///**
// * @author anelechila
// */
//
//@DataJpaTest
//public class UserSegmentRepositoryTest {
//
//    @Autowired
//    private UserRepository userRepository;
//
//    @Autowired
//    private SegmentRepository segmentRepository;
//
//    @Autowired
//    private TenantRepository tenantRepository;
//
//    @Autowired
//    private UserSegmentRepository userSegmentRepository;
//
//    private Tenant tenant;
//    private User user;
//    private Segment segment;
//
//
//
//    @BeforeEach
//    public void setUp() {
//        tenant = new Tenant();
//        tenant.createTenant(new CreateTenantRequest(anyString(), anyString(),anyString()));
//        tenant = tenantRepository.save(tenant);
//
//        user = new User(anyString(), tenant.getId());
//        user = userRepository.save(user);
//
//        segment = new Segment();
//        segment.createSegment(new CreateSegmentRequest(anyString(), anyString(), tenant.getId(), new Date(), new Date()));
//        segment = segmentRepository.save(segment);
//    }
//
//    @Test
//    public void testFindByUserId() {
//        //Given
//        UserSegment userSegment = new UserSegment();
//        userSegment.create(user, segment);
//
//        userSegmentRepository.save(userSegment);
//
//        //when
//        List<UserSegment> userSegments = userSegmentRepository.findByUserId(user.getId());
//
//        //then
//        //Assertions.assertFalse(userSegments.isEmpty());
//        //Assertions.assertNotNull(userSegments.get(0));
//        //Assertions.assertEquals(userSegments.get(0).getSegment(), segment);
//        //Assertions.assertEquals(userSegments.get(0).getUser(), user);
//    }
//
//    @Test
//    public void testFindByUserIdNotFound() {
//        //Given
//        UserSegment userSegment = new UserSegment();
//        userSegment.create(user, segment);
//
//        userSegmentRepository.save(userSegment);
//
//        //when
//        List<UserSegment> userSegments = userSegmentRepository.findByUserId(user.getId() + 1L);
//
//        //then
//        //Assertions.assertTrue(userSegments.isEmpty());
//
//    }
//
//    @Test
//    public void testFindBySegmentId() {
//        //Given
//        UserSegment userSegment = new UserSegment();
//        userSegment.create(user, segment);
//
//        userSegmentRepository.save(userSegment);
//
//        //when
//        List<UserSegment> userSegments = userSegmentRepository.findBySegmentId(user.getId());
//
//        //then
//        //Assertions.assertFalse(userSegments.isEmpty());
//        //Assertions.assertNotNull(userSegments.get(0));
//        //Assertions.assertEquals(userSegments.get(0).getSegment(), segment);
//        //Assertions.assertEquals(userSegments.get(0).getUser(), user);
//    }
//
//    @Test
//    public void testFindBySegmentIdNotFound() {
//        //Given
//        UserSegment userSegment = new UserSegment();
//        userSegment.create(user, segment);
//
//        userSegmentRepository.save(userSegment);
//
//        //when
//        List<UserSegment> userSegments = userSegmentRepository.findBySegmentId(user.getId() + 1L);
//
//        //then
//        //Assertions.assertTrue(userSegments.isEmpty());
//    }
//}
