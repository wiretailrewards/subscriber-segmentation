//package com.yoyo.subscribersegmentation.repository;
//
//import com.yoyo.subscribersegmentation.domain.models.CvsCampaign;
//import com.yoyo.subscribersegmentation.domain.models.Segment;
//import com.yoyo.subscribersegmentation.domain.models.Tenant;
//import com.yoyo.subscribersegmentation.domain.request.CreateCvsChannelRequest;
//import com.yoyo.subscribersegmentation.domain.request.CreateSegmentRequest;
//import com.yoyo.subscribersegmentation.domain.request.CreateTenantRequest;
//import org.junit.Ignore;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
//import org.springframework.dao.DataIntegrityViolationException;
//
//import java.util.Date;
//import java.util.List;
//
//import static org.mockito.ArgumentMatchers.anyString;
//
///**
// * @author anelechila
// */
//
//@DataJpaTest
//public class CvsCampaignRepositoryTest {
//
//    @Autowired
//    private CvsCampaignRepository cvsCampaignRepository;
//
//    @Autowired
//    private TenantRepository tenantRepository;
//
//    @Autowired
//    private SegmentRepository segmentRepository;
//
//    private CreateCvsChannelRequest request;
//
//    @BeforeEach
//    public void setUp() {
//        request = new CreateCvsChannelRequest(1l, "cvs-user", "cvs-password", 1l);
//    }
//
//    @Test
//    public void testWillThrowDataIntegrityViolationException() {
//        //given
//        CvsCampaign channel = new CvsCampaign();
//        channel.createChannel(request);
//
//        //when
//        //then
//        DataIntegrityViolationException exception = Assertions.assertThrows(DataIntegrityViolationException.class,
//                () -> cvsCampaignRepository.save(channel));
//
//        //Assertions.assertNotNull(exception);
//
//    }
//
//    @Test
//    public void testCvsChannelIsSaved() {
//        //given
//        Tenant tenant = new Tenant();
//        tenant.createTenant(new CreateTenantRequest(anyString(), anyString(), anyString()));
//        tenant = tenantRepository.save(tenant);
//
//        Assertions.assertNotNull(tenant);
//
//        Segment segment = new Segment();
//        segment.createSegment(new CreateSegmentRequest(anyString(), anyString(), tenant.getId(), new Date(), new Date()));
//        segmentRepository.save(segment);
//
//        Assertions.assertNotNull(segment);
//
//        CvsCampaign channel = new CvsCampaign();
//        channel.createChannel(request);
//
//        channel = cvsCampaignRepository.save(channel);
//
//        //Assertions.assertNotNull(channel);
//
//        //when
//        List<CvsCampaign> cvsCampaigns = cvsCampaignRepository.findBySegmentId(request.getSegmentId());
//
//
//        //then
//        //Assertions.assertFalse(cvsCampaigns.isEmpty());
//        //Assertions.assertEquals(cvsCampaigns.get(0).getSegmentId(), request.getSegmentId());
//    }
//
//}
