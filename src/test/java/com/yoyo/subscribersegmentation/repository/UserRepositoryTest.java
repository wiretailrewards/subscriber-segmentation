//package com.yoyo.subscribersegmentation.repository;
//
//import com.yoyo.subscribersegmentation.domain.models.Tenant;
//import com.yoyo.subscribersegmentation.domain.models.User;
//import com.yoyo.subscribersegmentation.domain.request.CreateTenantRequest;
//import org.junit.Ignore;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
//
//import static org.mockito.ArgumentMatchers.anyString;
//
///**
// * @author anelechila
// */
//
//@DataJpaTest
//public class UserRepositoryTest {
//
//    @Autowired
//    private UserRepository userRepository;
//
//    @Autowired
//    private TenantRepository tenantRepository;
//
//    private Tenant tenant;
//
//
//    @BeforeEach
//    public void setUp() {
//        tenant = new Tenant();
//        tenant.createTenant(new CreateTenantRequest(anyString(), anyString(), anyString()));
//        tenant = tenantRepository.save(tenant);
//    }
//
//    @Test
//    public void testFindByTenantUserId() {
//        //given
//        String user_id = "1";
//        User user = new User(user_id, tenant.getId());
//        userRepository.save(user);
//
//        //when
//        User userDb = userRepository.findByTenantUserId(user_id, tenant.getId());
//
//        //then
//        //Assertions.assertNotNull(userDb);
//        //Assertions.assertEquals(user, userDb);
//    }
//
//}
