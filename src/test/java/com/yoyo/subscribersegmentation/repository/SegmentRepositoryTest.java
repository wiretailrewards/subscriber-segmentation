//package com.yoyo.subscribersegmentation.repository;
//
//import com.yoyo.subscribersegmentation.domain.models.Segment;
//import com.yoyo.subscribersegmentation.domain.models.Tenant;
//import com.yoyo.subscribersegmentation.domain.request.CreateSegmentRequest;
//import com.yoyo.subscribersegmentation.domain.request.CreateTenantRequest;
//import org.junit.Ignore;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
//import org.springframework.dao.DataIntegrityViolationException;
//
//import java.util.Date;
//import java.util.List;
//import java.util.Random;
//
//import static org.mockito.ArgumentMatchers.anyLong;
//import static org.mockito.ArgumentMatchers.anyString;
//
///**
// * @author anelechila
// */
//
//@DataJpaTest
//public class SegmentRepositoryTest {
//
//    @Autowired
//    private TenantRepository tenantRepository;
//
//    @Autowired
//    private SegmentRepository segmentRepository;
//
//    private Tenant tenant;
//
//    @BeforeEach
//    public void setUp() {
//        tenant = new Tenant();
//        tenant.createTenant(new CreateTenantRequest(anyString(),anyString(), anyString()));
//        tenant = tenantRepository.save(tenant);
//    }
//
//    @Test
//    public void testWillThrowDataIntegrityViolationException() {
//        //given
//        Segment segment = new Segment();
//        segment.createSegment(new CreateSegmentRequest(anyString(), anyString(), anyLong(), new Date(), new Date()));
//
//        //when
//        //then
//        DataIntegrityViolationException exception = Assertions.assertThrows(DataIntegrityViolationException.class,
//                () -> segmentRepository.save(segment));
//
//        //Assertions.assertNotNull(exception);
//
//    }
//
//    @Test
//    public void testSegmentIsSaved() {
//        //given
//        Segment segment = new Segment();
//        segment.createSegment(new CreateSegmentRequest(anyString(), anyString(), tenant.getId(), new Date(), new Date()));
//
//        //when
//        segmentRepository.save(segment);
//
//        //then
//        //Assertions.assertNotNull(segment);
//    }
//
//    @Test
//    public void testFindByName() {
//        //given
//        Segment segment = new Segment();
//        segment.createSegment(new CreateSegmentRequest(anyString(), anyString(), tenant.getId(), new Date(), new Date()));
//
//        segmentRepository.save(segment);
//
//        //when
//        Segment segmentdb = segmentRepository.findByName(segment.getName(), tenant.getId().intValue());
//
//        //then
//        //Assertions.assertNotNull(segmentdb);
//        //Assertions.assertNotNull(segment.getId());
//    }
//
//    @Test
//    public void testFindDistinctBBybId() {
//        //given
//        Segment segment = new Segment();
//        segment.createSegment(new CreateSegmentRequest(anyString(), anyString(), tenant.getId(), new Date(), new Date()));
//
//        Segment segmentdb = segmentRepository.save(segment);
//
//        //when
//        segmentdb = segmentRepository.findDistinctById(segmentdb.getId(), tenant.getId().intValue());
//
//        //then
//        //Assertions.assertNotNull(segmentdb);
//        //Assertions.assertNotNull(segment.getId());
//    }
//
//    @Test
//    public void testFindDistinctBBybIdWrongTenantId() {
//        //given
//        Segment segment = new Segment();
//        segment.createSegment(new CreateSegmentRequest(anyString(), anyString(), tenant.getId(), new Date(), new Date()));
//
//        Segment segmentdb = segmentRepository.save(segment);
//
//        //when
//        segmentdb = segmentRepository.findDistinctById(segmentdb.getId(), new Random().nextInt());
//
//        //then
//        //Assertions.assertNull(segmentdb);
//    }
//
//    @Test
//    public void testFindByTenantId() {
//        //given
//        Segment segment = new Segment();
//        segment.createSegment(new CreateSegmentRequest(anyString(), anyString(), tenant.getId(), new Date(), new Date()));
//
//        Segment segmentdb = segmentRepository.save(segment);
//
//        //when
//        List<Segment> segments = segmentRepository.findByTenantId(tenant.getId().intValue());
//
//        //then
//        //Assertions.assertNotNull(segments.get(0));
//    }
//}
