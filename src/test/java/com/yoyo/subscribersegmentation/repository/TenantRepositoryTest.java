//package com.yoyo.subscribersegmentation.repository;
//
//import com.yoyo.subscribersegmentation.domain.models.Tenant;
//import com.yoyo.subscribersegmentation.domain.request.CreateTenantRequest;
//import org.junit.Ignore;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
//
//import static org.mockito.ArgumentMatchers.anyString;
//
///**
// * @author anelechila
// */
//
//@DataJpaTest
//public class TenantRepositoryTest {
//
//    @Autowired
//    private TenantRepository tenantRepository;
//
//    private String name;
//    private String password;
//
//    @BeforeEach
//    public void setUp() {
//        name = "tenantName";
//        password = "tenantPassword";
//    }
//
//    @Test
//    public void testGetTenantByCredentials() {
//        //given
//        Tenant tenant = new Tenant();
//        tenant.createTenant(new CreateTenantRequest(name, password, anyString()));
//        tenant = tenantRepository.save(tenant);
//
//        //Assertions.assertNotNull(tenant);
//
//        //when
//        Tenant tenantDb = tenantRepository.getTenantByCredentials(name, password);
//
//        //then
//        //Assertions.assertNotNull(tenantDb);
//        //Assertions.assertEquals(tenant.getId(), tenantDb.getId());
//    }
//
//    @Test
//    public void testGetTenantByCredentialsNotFound() {
//        //given
//        Tenant tenant = new Tenant();
//        tenant.createTenant(new CreateTenantRequest(name, password, anyString()));
//        tenant = tenantRepository.save(tenant);
//
//        Assertions.assertNotNull(tenant);
//
//        //when
//        Tenant tenantDb = tenantRepository.getTenantByCredentials(name + "not_found", password);
//
//        //then
//        //Assertions.assertNull(tenantDb);
//    }
//}
