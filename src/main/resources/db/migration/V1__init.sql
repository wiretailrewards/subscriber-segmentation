-- ==================================================================================
-- V1__init.sql
--
-- This file contains the base scheme for the application.
-- Note that H2 does not support named foreign keys. Therefore,foreign keys are not named, to
-- be compatible with both MySQL and H2.
-- ==================================================================================

CREATE TABLE IF NOT EXISTS `tenant` (
`id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
`name` varchar(255)  NOT NULL,
`password` varchar(255)  NOT NULL,
`application_key` varchar(255)  NOT NULL,
`create_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
`last_modified_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
`state` varchar(45) NOT NULL,
PRIMARY KEY (`id`)
)ENGINE =InnoDB DEFAULT CHARSET =utf8mb4;

CREATE UNIQUE INDEX ID_tenant_name ON tenant(`name`);

CREATE TABLE IF NOT EXISTS `user` (
  `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_ref` VARCHAR(255) NOT NULL,
  `tenant_id` INTEGER UNSIGNED NOT NULL,
  `user_alias` VARCHAR(255),
  `create_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `state` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `tenant_ifblk_1` FOREIGN KEY (`tenant_id`) REFERENCES `tenant` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `segment` (
    `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `description` VARCHAR(255) NOT NULL,
    `tenant_id` INTEGER UNSIGNED NOT NULL,
    `start_date` DATETIME NOT NULL,
    `end_date` DATETIME NOT NULL,
    `create_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `last_modified_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `state` varchar(45) NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `tenant_ifblk_2` FOREIGN KEY (`tenant_id`) REFERENCES `tenant` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE INDEX ID_channel_tenantid ON segment(`tenant_id`);

CREATE TABLE IF NOT EXISTS `cvs_campaign` (
  `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(255)  NOT NULL,
  `password` varchar(255)  NOT NULL,
  `segment_id` INTEGER UNSIGNED NOT NULL,
  `create_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `state` varchar(45) NOT NULL,
  `campaign_id` INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `segment_ifblk_1` FOREIGN KEY (`segment_id`) REFERENCES `segment` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE INDEX ID_cvs_campaign_lastmodified ON cvs_campaign(`last_modified_date`);

CREATE TABLE IF NOT EXISTS `user_segment` (
    `user_id` INTEGER UNSIGNED NOT NULL,
    `segment_id` INTEGER UNSIGNED NOT NULL,
    `create_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `last_modified_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `state` varchar(45) NOT NULL,
    PRIMARY KEY (`user_id`,`segment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `config`
(
    `id`           varchar(255) NOT NULL,
    `date_created` timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `last_updated` timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `description`  varchar(255) NOT NULL,
    `type`         varchar(255) NOT NULL,
    `value`        varchar(755) NOT NULL,
    `state` varchar(45) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
