package com.yoyo.subscribersegmentation.exception.errors;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author anelechila
 */

public class ErrorResponse {
    private String code;
    private String message;
    private List<String> globalErrors;
    private List<ErrorField> errors = new ArrayList<>();

    public ErrorResponse() { }

    public ErrorResponse(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() { return code; }

    public void setCode(String code) { this.code = code; }

    public String getMessage() { return message; }

    public void setMessage(String message) { this.message = message; }

    public List<ErrorField> getErrors() {
        return errors;
    }

    public void setErrors(List<ErrorField> errors) {
        this.errors = errors;
    }

    public void addGlobalError(String message) {
    	if (globalErrors == null) {
    		globalErrors = new LinkedList<String>();
    	}
    	
    	globalErrors.add(message);
    }

    public void addError(ErrorField error) {
        if (!alReadyAdded(error)) {
            errors.add(error);
        }
    }

    private boolean alReadyAdded(ErrorField error) {
        for (ErrorField errorField : errors) {
            if (errorField.equals(error)) {
                return true;
            }
        }

        return false;
    }
}