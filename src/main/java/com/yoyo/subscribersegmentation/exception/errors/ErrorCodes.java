package com.yoyo.subscribersegmentation.exception.errors;

import org.springframework.http.HttpStatus;

/**
 * @author anelechila
 */

public enum ErrorCodes {
    // ==============================================================================================================================
    // GENERAL ERRORS
    // ==============================================================================================================================
    GENERAL_SYSTEM_ERR(1, "General System Error", HttpStatus.INTERNAL_SERVER_ERROR),
    GENERAL_DATABASE_ERR(2, "General Database Error", HttpStatus.INTERNAL_SERVER_ERROR),


    // ==============================================================================================================================
    // GENERAL API ERRORS
    // ==============================================================================================================================
    INVALID_ID_VALUE(3, "Invalid value for 'id' parameter.", HttpStatus.BAD_REQUEST),
    FIELD_VALIDATION_ERR(4, "Invalid field: ", HttpStatus.BAD_REQUEST),
    INVALID_REQUEST(5, "Invalid Request", HttpStatus.BAD_REQUEST),
    DUPLICATE_FIELD(6, "Duplicate field", HttpStatus.BAD_REQUEST),
    AUTHENTICATION_FAILED(7,"Authentication failed.",HttpStatus.UNAUTHORIZED),
    BAD_REQUEST(9, "Bad Request", HttpStatus.BAD_REQUEST),


    // ==============================================================================================================================
    // ENTITY NOT FOUND ERRORS
    // ==============================================================================================================================
    NOT_FOUND_CVS_CHANNEL(10,"Entity does not exist: CVS_CHANNEL",HttpStatus.BAD_REQUEST),
    NOT_FOUND_SEGMENT(11,"Entity does not exist: SEGMENT",HttpStatus.BAD_REQUEST),
    NOT_FOUND_TENANT(12,"Entity does not exist: TENANT",HttpStatus.BAD_REQUEST),
    NOT_FOUND_USER(13,"Entity does not exist: USER",HttpStatus.BAD_REQUEST),
    NOT_FOUND_CONFIG(14,"Entity does not exist: CONFIG",HttpStatus.BAD_REQUEST);


    // ==============================================================================================================================
    private String responseCode;
    private String responseDesc;
    private HttpStatus httpStatus;
    private static final String RESPONSE_CODE_FORMAT = "%04d";

    ErrorCodes(int responseCode, String responseDesc, HttpStatus httpStatus) {
        if (responseCode >= 0) {
            this.responseCode = String.format(RESPONSE_CODE_FORMAT, responseCode);
        } else {
            this.responseCode = String.valueOf(responseCode);
        }
        this.responseDesc = responseDesc;
        this.httpStatus = httpStatus;
    }


    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseDesc() {
        return responseDesc;
    }

    public void setResponseDesc(String responseDesc) {
        this.responseDesc = responseDesc;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
}
