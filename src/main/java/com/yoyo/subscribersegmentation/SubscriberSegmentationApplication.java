package com.yoyo.subscribersegmentation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
public class SubscriberSegmentationApplication {

    public static void main(String[] args) {
        SpringApplication.run(SubscriberSegmentationApplication.class, args);
    }

}
