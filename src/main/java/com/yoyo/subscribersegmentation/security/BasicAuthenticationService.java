package com.yoyo.subscribersegmentation.security;

import com.yoyo.subscribersegmentation.domain.models.Tenant;
import com.yoyo.subscribersegmentation.exception.AuthenticationException;
import com.yoyo.subscribersegmentation.exception.errors.ErrorCodes;
import com.yoyo.subscribersegmentation.repository.TenantRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * @author anelechila
 */

@Service
@NoArgsConstructor
public class BasicAuthenticationService implements UserDetailsService {

    private TenantRepository tenantRepository;

    @Autowired
    public BasicAuthenticationService(TenantRepository tenantRepository) {
        this.tenantRepository = tenantRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Tenant tenant = tenantRepository.getTenantByName(username);
        if (tenant == null) {
            throw new AuthenticationException(ErrorCodes.AUTHENTICATION_FAILED.getResponseDesc());
        }

        return new User(tenant.getName(), tenant.getPassword(), new ArrayList<>());
    }
}
