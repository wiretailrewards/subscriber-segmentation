package com.yoyo.subscribersegmentation.security;

import com.yoyo.subscribersegmentation.utils.HashUtils;
import com.yoyo.subscribersegmentation.exception.AuthenticationException;
import com.yoyo.subscribersegmentation.exception.errors.ErrorCodes;
import com.yoyo.subscribersegmentation.handler.SEGMENTATIONLOGGER;
import com.yoyo.subscribersegmentation.repository.TenantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
@WebFilter(urlPatterns = {"/admin/**"})
public class ApiKeyAuthenticationFilter implements Filter {

    private final TenantRepository tenantRepository;
    private static final List<String> excludedPaths = new ArrayList<>();

    @Autowired
    public ApiKeyAuthenticationFilter(TenantRepository tenantRepository) {
        this.tenantRepository = tenantRepository;
    }

    @Override
    @SEGMENTATIONLOGGER
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws ServletException, IOException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        String apiKey = request.getHeader("apiKey");
        String path = request.getServletPath();

        if(path.contains("/admin") || !excludedPaths.contains(path)) {
            authenticate(response, apiKey);
        }

        filterChain.doFilter(request, response);
    }

    private void authenticate(HttpServletResponse response, String apiKey) {
        if (tenantRepository.getTenantByApiKey(HashUtils.toSHA256(apiKey)) == null) {
            response.setStatus(ErrorCodes.AUTHENTICATION_FAILED.getHttpStatus().value());
            response.setContentType("application/json");
            throw new AuthenticationException(ErrorCodes.AUTHENTICATION_FAILED.getResponseDesc());
        }
    }

    static {
        excludedPaths.add("/admin");
        excludedPaths.add("/swagger-ui.html");
        excludedPaths.add("/");
        excludedPaths.add("/login");
        excludedPaths.add("/v2/api-docs");
        excludedPaths.add("/segments");
        excludedPaths.add("/tenants");
        excludedPaths.add("/admin/tenants");
    }
}
