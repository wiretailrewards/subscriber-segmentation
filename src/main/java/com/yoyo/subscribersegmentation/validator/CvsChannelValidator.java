package com.yoyo.subscribersegmentation.validator;

import com.yoyo.subscribersegmentation.exception.InvalidRequestException;
import com.yoyo.subscribersegmentation.domain.request.CreateCvsChannelRequest;
import com.yoyo.subscribersegmentation.domain.request.UpdateCvsChannelRequest;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static com.yoyo.subscribersegmentation.exception.errors.ErrorCodes.FIELD_VALIDATION_ERR;
import static com.yoyo.subscribersegmentation.exception.errors.ErrorCodes.INVALID_REQUEST;

/**
 * @author anelechila
 */

@Component
public class CvsChannelValidator implements Validator {

    @Override
    public void validate(Object obj, Errors errors) {

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        javax.validation.Validator validator = factory.getValidator();
        CreateCvsChannelRequest serverRequest = (CreateCvsChannelRequest) obj;

        try {
            Set<ConstraintViolation<CreateCvsChannelRequest>> violations = validator.validate(serverRequest);
            for(ConstraintViolation<CreateCvsChannelRequest> violation  : violations) {
                errors.rejectValue(violation.getPropertyPath().toString(), FIELD_VALIDATION_ERR.getResponseCode(), violation.getMessage());
            }

        } catch (Exception e) {
            throw new InvalidRequestException(INVALID_REQUEST.getResponseCode(), errors);
        }
    }

    public void validateForUpdate(Object obj, Errors errors) {

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        javax.validation.Validator validator = factory.getValidator();
        UpdateCvsChannelRequest serverRequest = (UpdateCvsChannelRequest) obj;

        try {
            Set<ConstraintViolation<UpdateCvsChannelRequest>> violations = validator.validate(serverRequest);
            for(ConstraintViolation<UpdateCvsChannelRequest> violation  : violations) {
                errors.rejectValue(violation.getPropertyPath().toString(), FIELD_VALIDATION_ERR.getResponseCode(), violation.getMessage());
            }

        } catch (Exception e) {
            throw new InvalidRequestException(INVALID_REQUEST.getResponseCode(), errors);
        }
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return CreateCvsChannelRequest.class.isAssignableFrom(clazz);
    }
}
