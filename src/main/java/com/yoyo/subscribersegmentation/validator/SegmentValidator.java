package com.yoyo.subscribersegmentation.validator;

import com.yoyo.subscribersegmentation.exception.InvalidRequestException;
import com.yoyo.subscribersegmentation.domain.request.CreateSegmentRequest;
import com.yoyo.subscribersegmentation.domain.request.UpdateSegmentRequest;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static com.yoyo.subscribersegmentation.exception.errors.ErrorCodes.FIELD_VALIDATION_ERR;
import static com.yoyo.subscribersegmentation.exception.errors.ErrorCodes.INVALID_REQUEST;

/**
 * @author anelechila
 */

@Component
public class SegmentValidator implements Validator {

    @Override
    public void validate(Object obj, Errors errors) {

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        javax.validation.Validator validator = factory.getValidator();
        CreateSegmentRequest serverRequest = (CreateSegmentRequest) obj;

        try {
            Set<ConstraintViolation<CreateSegmentRequest>> violations = validator.validate(serverRequest);
            for(ConstraintViolation<CreateSegmentRequest> violation  : violations) {
                errors.rejectValue(violation.getPropertyPath().toString(), FIELD_VALIDATION_ERR.getResponseCode(), violation.getMessage());
            }

        } catch (Exception e) {
            throw new InvalidRequestException(INVALID_REQUEST.getResponseCode(), errors);
        }
    }

    public void validateForUpdate(Object obj, Errors errors) {

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        javax.validation.Validator validator = factory.getValidator();
        UpdateSegmentRequest serverRequest = (UpdateSegmentRequest) obj;

        try {
            Set<ConstraintViolation<UpdateSegmentRequest>> violations = validator.validate(serverRequest);
            for(ConstraintViolation<UpdateSegmentRequest> violation  : violations) {
                errors.rejectValue(violation.getPropertyPath().toString(), FIELD_VALIDATION_ERR.getResponseCode(), violation.getMessage());
            }

        } catch (Exception e) {
            throw new InvalidRequestException(INVALID_REQUEST.getResponseCode(), errors);
        }
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return CreateSegmentRequest.class.isAssignableFrom(clazz);
    }
}
