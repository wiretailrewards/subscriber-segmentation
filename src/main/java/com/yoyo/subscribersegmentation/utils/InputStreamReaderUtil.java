package com.yoyo.subscribersegmentation.utils;

import com.yoyo.subscribersegmentation.exception.ApiException;
import io.micrometer.core.instrument.util.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InputStreamReaderUtil {



    private List<String> readFromInputStream(InputStream inputStream)
            throws IOException {
        String COMMA_DELIMITER = ",";
        List<String> mobileNumbers = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(COMMA_DELIMITER);
                mobileNumbers.addAll(Arrays.asList(values));
            }
        } finally {
            if (inputStream != null) {
                try {
                    //inputStream.close();
                    inputStream.getClass();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return mobileNumbers;
    }
}
