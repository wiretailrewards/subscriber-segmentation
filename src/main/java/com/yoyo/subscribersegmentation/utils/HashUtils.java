package com.yoyo.subscribersegmentation.utils;
import com.yoyo.subscribersegmentation.exception.ApiException;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public class HashUtils {

    public static String toSHA256(String plain) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-256");
            md.update(plain.getBytes(StandardCharsets.UTF_8));
        } catch (Exception var3) {
            throw new ApiException("The PIN could not be hashed.");
        }

        BigInteger hex = new BigInteger(1, md.digest());
        return hex.toString(16);
    }

    public static String toSHA1(String plain) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-1");
            md.update(plain.getBytes(StandardCharsets.UTF_8));
        } catch (Exception var3) {
            throw new ApiException("The PIN could not be hashed.");
        }

        BigInteger hex = new BigInteger(1, md.digest());
        return hex.toString(16);
    }
}
