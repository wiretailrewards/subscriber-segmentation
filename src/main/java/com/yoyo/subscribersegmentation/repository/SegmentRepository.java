package com.yoyo.subscribersegmentation.repository;

import com.yoyo.subscribersegmentation.domain.models.Segment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author anelechila
 */

@Repository
public interface SegmentRepository extends JpaRepository<Segment, Long> {

    @Query("SELECT s FROM Segment s where s.name = :name and s.tenantId = :tenantId")
    Segment findByName(@Param("name") String name, @Param("tenantId") Integer tenantId);

    @Query("SELECT s FROM Segment s where s.id = :id and s.tenantId = :tenantId")
    Segment findDistinctById(@Param("id") Long id, @Param("tenantId") Integer tenantId);

    @Query("SELECT s FROM Segment s where s.tenantId = :tenantId")
    List<Segment> findByTenantId(@Param("tenantId") Integer tenantId);
}
