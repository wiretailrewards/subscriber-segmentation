package com.yoyo.subscribersegmentation.repository;

import com.yoyo.subscribersegmentation.domain.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author anelechila
 */

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query("SELECT u FROM User u WHERE u.userRef = :userRef AND u.tenantId = :tenantId")
    User findByTenantUserId(@Param("userRef") String userRef,@Param("tenantId") Long tenantId );
}
