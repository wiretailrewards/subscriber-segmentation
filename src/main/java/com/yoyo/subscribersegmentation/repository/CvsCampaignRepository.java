package com.yoyo.subscribersegmentation.repository;

import com.yoyo.subscribersegmentation.domain.models.CvsCampaign;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author anelechila
 */

@Repository
public interface CvsCampaignRepository extends JpaRepository<CvsCampaign, Long> {

    @Query("SELECT cc FROM CvsCampaign cc WHERE cc.segmentId = :segmentId")
    List<CvsCampaign> findBySegmentId(@Param("segmentId") Long segmentId);
}
