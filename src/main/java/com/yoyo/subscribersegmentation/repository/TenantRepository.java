package com.yoyo.subscribersegmentation.repository;

import com.yoyo.subscribersegmentation.domain.models.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author anelechila
 */

@Repository
public interface TenantRepository extends JpaRepository<Tenant, Long> {

    @Query(value = "SELECT t FROM Tenant t WHERE t.name = :name AND t.password = :password")
    Tenant getTenantByCredentials(@Param("name") String name, @Param("password") String password);

    @Query(value = "SELECT t FROM Tenant t WHERE t.name = :name")
    Tenant getTenantByName(@Param("name") String name);

    @Query(value = "SELECT t FROM Tenant t WHERE t.applicationKey = :applicationKey")
    Tenant getTenantByApiKey(@Param("applicationKey") String applicationKey);
}
