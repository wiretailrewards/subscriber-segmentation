package com.yoyo.subscribersegmentation.repository;

import com.yoyo.subscribersegmentation.domain.models.UserSegment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserSegmentRepository extends JpaRepository<UserSegment, Long> {


    @Query("SELECT u FROM UserSegment u WHERE u.id.userId = :userId")
    List<UserSegment> findByUserId(@Param("userId") Long userId);

    @Query("SELECT u FROM UserSegment u WHERE u.id.segmentId = :segmentId")
    List<UserSegment> findBySegmentId(@Param("segmentId") Long segmentId);
}
