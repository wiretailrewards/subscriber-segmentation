package com.yoyo.subscribersegmentation.repository;

import com.yoyo.subscribersegmentation.domain.models.Config;
import com.yoyo.subscribersegmentation.domain.models.CvsCampaign;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author anelechila
 */

@Repository
public interface ConfigRepository extends JpaRepository<Config, String> {
}
