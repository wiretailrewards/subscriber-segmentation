package com.yoyo.subscribersegmentation.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

public enum ConfigId {
    TARGETED_MESSAGING_S3_BUCKET_NAME
}
