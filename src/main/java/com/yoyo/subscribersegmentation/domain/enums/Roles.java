package com.yoyo.subscribersegmentation.domain.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Roles {

    ADMIN("Admin"),
    TENANT("Tenant");

    private final String role;
}
