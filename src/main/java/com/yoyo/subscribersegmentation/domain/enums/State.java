package com.yoyo.subscribersegmentation.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum State {

    ACTIVE("A"),
    DEACTIVATED("D"),
    DELETED("DEL");

    private String state;
}
