package com.yoyo.subscribersegmentation.domain.response;

import com.yoyo.subscribersegmentation.domain.models.CvsCampaign;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CvsCampaignResponse extends ServerResponse {

    private Long segmentId;
    private String username;
    private String password;
    private Long campaignId;
    private String state;
    private Date createDate;
    private Date lastModifiedDate;

    public CvsCampaignResponse(CvsCampaign cvsCampaign) {
        this.password = cvsCampaign.getPassword();
        this.segmentId = cvsCampaign.getSegmentId();
        this.username = cvsCampaign.getUsername();
        this.campaignId = cvsCampaign.getCampaignId();
        this.state = cvsCampaign.getState();
        this.createDate = new Date();
        this.lastModifiedDate = new Date();
    }

}
