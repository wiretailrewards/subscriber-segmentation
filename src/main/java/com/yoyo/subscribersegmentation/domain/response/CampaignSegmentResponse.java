package com.yoyo.subscribersegmentation.domain.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CampaignSegmentResponse extends ServerResponse {

    private SegmentResponse segment;
    private List<CvsCampaignResponse> cvsCampaigns;
}
