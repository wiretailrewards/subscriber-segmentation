package com.yoyo.subscribersegmentation.domain.response;

import com.yoyo.subscribersegmentation.domain.models.Segment;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SegmentResponse extends ServerResponse {

    private String name;
    private String description;
    private Integer tenantId;
    private Date startDate;
    private Date endDate;
    private String state;
    private Date createDate;
    private Date lastModifiedDate;
    private int count;


    public SegmentResponse(Segment segment) {
        this.name = segment.getName();
        this.tenantId = segment.getTenantId();
        this.startDate = segment.getStartDate();
        this.endDate = segment.getEndDate();
        this.state = segment.getState();
        this.lastModifiedDate = segment.getLastModifiedDate();
        this.createDate  = segment.getCreateDate();
        this.description = segment.getDescription();
    }
}
