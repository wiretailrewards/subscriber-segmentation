package com.yoyo.subscribersegmentation.domain.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author anelechila
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GetUsersResponse extends ServerResponse {

    private List<UserResponse> users = new ArrayList<>();
}
