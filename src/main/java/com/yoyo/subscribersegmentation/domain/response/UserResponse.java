package com.yoyo.subscribersegmentation.domain.response;

import com.yoyo.subscribersegmentation.domain.models.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

/**
 * @author anelechila
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserResponse extends ServerResponse {

    private String userRef;
    private Long tenantId;
    private String userAlias;
    private String state;
    private Date createDate;
    private Date lastModifiedDate;

    public UserResponse(User user) {
        this.createDate = user.getCreateDate();
        this.lastModifiedDate = user.getLastModifiedDate();
        this.state = user.getState();
        this.tenantId = user.getTenantId();
        this.userAlias = user.getUserAlias();
        this.userRef = user.getUserRef();
    }
}
