package com.yoyo.subscribersegmentation.domain.response;

import com.yoyo.subscribersegmentation.domain.models.Tenant;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TenantResponse extends ServerResponse {

    private Long id;
    private String name;
    private String password;
    private Date createDate;
    private Date lastModifiedDate;
    private String state;
    private String applicationKey;

    public TenantResponse(Tenant tenant) {
        this.id = tenant.getId();
        this.name = tenant.getName();
        this.password = tenant.getPassword();
        this.state = tenant.getState();
        this.lastModifiedDate = tenant.getLastModifiedDate();
        this.createDate  = tenant.getCreateDate();
        this.applicationKey = tenant.getApplicationKey();
    }

}
