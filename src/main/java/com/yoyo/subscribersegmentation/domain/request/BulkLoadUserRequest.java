package com.yoyo.subscribersegmentation.domain.request;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author anelechila
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BulkLoadUserRequest {

    @NotEmpty(message = "User id's are required")
    private String fileName;

    @NotNull(message = "Segment name is required")
    private Long segmentId;

    @NotNull(message = " is required")
    private Long tenantId;

}
