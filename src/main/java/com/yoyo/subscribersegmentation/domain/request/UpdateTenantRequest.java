package com.yoyo.subscribersegmentation.domain.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Size;

/**
 * @author anelechila
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UpdateTenantRequest {

    @Size(min = 1, message = "Name cannot be empty")
    private String name;
    @Size(min = 1, message = "Password cannot be empty")
    private String password;
    @Size(min = 1, message = "State cannot be empty")
    private String state;
    @Size(min = 1, message = "Application Key cannot be empty")
    private String applicationKey;
    @JsonIgnore
    private Long id;
}
