package com.yoyo.subscribersegmentation.domain.request;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * @author anelechila
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BulkDeleteAllUsersRequest {

    @NotNull(message = "Segment name is required")
    private Long segmentId;

    @NotNull(message = " is required")
    private Long tenantId;

}
