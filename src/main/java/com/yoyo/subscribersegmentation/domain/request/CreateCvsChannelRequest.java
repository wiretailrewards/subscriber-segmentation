package com.yoyo.subscribersegmentation.domain.request;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

/**
 * @author anelechila
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateCvsChannelRequest {

    //Todo not required, first check tenant exists
    @Positive(message = "Tenant id must be a positive value")
    private Long segmentId;

    @NotBlank(message = "username is required")
    private String username;

    @NotBlank(message = "password is required")
    private String password;

    @Positive(message = "Campaign id id must be a positive value")
    private Long campaignId;

}
