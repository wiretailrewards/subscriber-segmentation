package com.yoyo.subscribersegmentation.domain.request;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author anelechila
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BulkDeleteUserRequest {

    @NotEmpty(message = "User refs are required")
    private List<String> userRef;

    @NotNull(message = "Segment name is required")
    private Long segmentId;

    @NotNull(message = " is required")
    private Long tenantId;

}
