package com.yoyo.subscribersegmentation.domain.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * @author anelechila
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateTenantRequest {

    @NotBlank(message = "name is required")
    private String name;
    @NotBlank(message = "password is required")
    private String password;
    @NotBlank(message = "applicationKey is required")
    private String applicationKey;

}
