package com.yoyo.subscribersegmentation.domain.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Date;

/**
 * @author anelechila
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateSegmentRequest {

    @NotBlank(message = "name is required")
    private String name;

    @NotBlank(message = "name is required")
    private String description;

    private @Positive(message = "Tenant id must be a positive value") Long tenantId;

    @NotNull(message = "StartDate is required")
    private Date startDate;

    @NotNull(message = "End date is required")
    private Date endDate;
}
