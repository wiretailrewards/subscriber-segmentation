package com.yoyo.subscribersegmentation.domain.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * @author anelechila
 */


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UpdateSegmentRequest {

    private Date startDate;
    private Date endDate;

    @Size(min = 1, message = "Name cannot be empty")
    private String name;

    @Size(min = 1, message = "Name cannot be empty")
    private String description;

    @Positive(message = "Tenant id must be a positive value")
    private Integer tenantId;

    @Size(min = 1, message = "State cannot be empty")
    private String state;

    @JsonIgnore
    private Long id;
}
