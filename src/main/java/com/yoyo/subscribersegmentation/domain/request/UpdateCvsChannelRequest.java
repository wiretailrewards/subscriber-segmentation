package com.yoyo.subscribersegmentation.domain.request;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

/**
 * @author anelechila
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UpdateCvsChannelRequest {

    //Todo not required, first check tenant exists
    @Positive(message = "Tenant id must be a positive value")
    private Long segmentId;

    @Size(min = 1, message = "Username cannot be empty")
    private String username;

    @Size(min = 1, message = "Password cannot be empty")
    private String password;

    @Positive(message = "Campaign id id must be a positive value")
    private Long campaignId;

    @Size(min = 1, message = "State cannot be empty")
    private String state;

    @JsonIgnore
    private Long id;

}
