package com.yoyo.subscribersegmentation.domain.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "config")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Config extends BaseEntity {

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private String value;

    @Column(nullable = false)
    private String type;
}
