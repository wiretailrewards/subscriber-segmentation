package com.yoyo.subscribersegmentation.domain.models;


import com.yoyo.subscribersegmentation.domain.enums.State;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author anelechila
 */

@Entity
@Table(name = "user")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class User extends BaseEntity {

    @Column(nullable = false, name = "user_ref")
    private String userRef;

    @Column(nullable = false, name = "tenant_id", columnDefinition = "INTEGER")
    private Long tenantId;

    @Column(nullable = true, name = "user_alias")
    private String userAlias;

    public User(String userRef, Long tenantId) {
        this.tenantId = tenantId;
        this.userRef = userRef;
        this.setLastModifiedDate(new Date());
        this.setCreateDate(new Date());
        this.setState(State.ACTIVE.getState());
    }
}
