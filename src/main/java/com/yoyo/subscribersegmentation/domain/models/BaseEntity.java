package com.yoyo.subscribersegmentation.domain.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * @author anelechila
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@MappedSuperclass
public class BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(
            columnDefinition = "INTEGER"
    )
    private Long id;

    @Column(nullable = false)
    private Date createDate;

    @Column(nullable = false)
    private Date lastModifiedDate;

    @Column(nullable = false)
    private String state;

}
