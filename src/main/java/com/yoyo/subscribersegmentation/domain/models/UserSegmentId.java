package com.yoyo.subscribersegmentation.domain.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author anelechila
 */

@Embeddable
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UserSegmentId implements Serializable {

    @Column(nullable =false)
    Long userId;

    @Column(nullable =false)
    Long segmentId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserSegmentId that = (UserSegmentId) o;
        return Objects.equals(segmentId, that.segmentId) &&
                Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(segmentId, userId);
    }
}
