package com.yoyo.subscribersegmentation.domain.models;

import com.yoyo.subscribersegmentation.domain.enums.State;
import com.yoyo.subscribersegmentation.domain.request.CreateSegmentRequest;
import com.yoyo.subscribersegmentation.domain.request.UpdateSegmentRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;


/**
 * @author anelechila
 */

@Entity
@Table(name = "segment")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Segment extends BaseEntity {

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false, name = "tenant_id")
    private Integer tenantId;

    @Column(nullable = false, name = "start_date")
    private Date startDate;

    @Column(nullable = false, name = "end_date")
    private Date endDate;

    public void createSegment(CreateSegmentRequest request) {
        this.name = request.getName();
        this.description = request.getDescription();
        this.tenantId = request.getTenantId().intValue();
        this.startDate = request.getStartDate();
        this.endDate = request.getEndDate();
        this.setState(State.ACTIVE.getState());
        this.setCreateDate(new Date());
        this.setLastModifiedDate(new Date());
    }

    public void updateSegment(UpdateSegmentRequest request) {
        this.name = request.getName() == null ? this.name : request.getName();
        this.description = request.getDescription() == null ? this.description : request.getDescription();
        this.tenantId = request.getTenantId() == null ? this.tenantId : request.getTenantId();
        this.startDate = request.getStartDate() == null ? this.startDate : request.getStartDate();
        this.endDate = request.getEndDate() == null ? this.endDate : request.getEndDate();
        this.setState(request.getState() == null ? this.getState() : request.getState());
        this.setLastModifiedDate(new Date());
    }
}
