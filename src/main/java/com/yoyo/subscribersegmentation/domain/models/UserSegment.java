package com.yoyo.subscribersegmentation.domain.models;

import com.yoyo.subscribersegmentation.domain.enums.State;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * @author anelechila
 */

@Entity
@Table(name = "user_segment")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserSegment {

    @EmbeddedId
    UserSegmentId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("userId")
    User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("segmentId")
    Segment segment;

    @Column(nullable = false)
    private Date createDate;

    @Column(nullable = false)
    private Date lastModifiedDate;

    @Column(nullable = false)
    private String state;

    public void create(User user, Segment segment) {
        this.createDate = new Date();
        this.lastModifiedDate = new Date();
        this.state = State.ACTIVE.getState();
        this.user = user;
        this.segment = segment;
        this.id = new UserSegmentId(user.getId(), segment.getId());
    }
}
