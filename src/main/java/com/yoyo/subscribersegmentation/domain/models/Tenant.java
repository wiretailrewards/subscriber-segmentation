package com.yoyo.subscribersegmentation.domain.models;

import com.yoyo.subscribersegmentation.utils.HashUtils;
import com.yoyo.subscribersegmentation.domain.enums.State;
import com.yoyo.subscribersegmentation.domain.request.CreateTenantRequest;
import com.yoyo.subscribersegmentation.domain.request.UpdateTenantRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author anelechila
 */

@Entity
@Table(name = "tenant")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
//Todo - must be changed to 'application'
public class Tenant extends BaseEntity {

    @Column(unique = true, nullable = false)
    private String name;
    @Column(nullable = false)
    private String password;
    @Column(unique = true, nullable = false)
    private String applicationKey;

    public void createTenant(CreateTenantRequest request) {
        this.name = request.getName();
        this.password = HashUtils.toSHA1(request.getPassword());
        this.applicationKey = HashUtils.toSHA256(request.getApplicationKey());
        this.setState(State.ACTIVE.getState());
        this.setCreateDate(new Date());
        this.setLastModifiedDate(new Date());
    }

    public void updateTenant(UpdateTenantRequest request) {
        this.name = request.getName() == null ? this.name : request.getName();
        this.password = request.getPassword() == null ? this.password : HashUtils.toSHA1(request.getPassword());
        this.applicationKey = request.getApplicationKey() == null ? this.applicationKey : HashUtils.toSHA256(request.getApplicationKey());
        this.setState(request.getState() == null ? this.getState() : request.getState());
        this.setLastModifiedDate(new Date());
    }

}
