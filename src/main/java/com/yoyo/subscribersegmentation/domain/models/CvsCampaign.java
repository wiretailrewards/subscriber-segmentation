package com.yoyo.subscribersegmentation.domain.models;


import com.yoyo.subscribersegmentation.utils.HashUtils;
import com.yoyo.subscribersegmentation.domain.enums.State;
import com.yoyo.subscribersegmentation.domain.request.CreateCvsChannelRequest;
import com.yoyo.subscribersegmentation.domain.request.UpdateCvsChannelRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author anelechila
 */

@Entity
@Table(name = "cvs_campaign")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CvsCampaign extends BaseEntity {

    @Column(nullable = false, name = "segment_id", columnDefinition = "INTEGER")
    private Long segmentId;

    @Column(nullable = false)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private Long campaignId;

    public void createChannel(CreateCvsChannelRequest request) {
        this.password = HashUtils.toSHA1(request.getPassword());
        this.segmentId = request.getSegmentId();
        this.username = request.getUsername();
        this.campaignId = request.getCampaignId();
        this.setState(State.ACTIVE.getState());
        this.setCreateDate(new Date());
        this.setLastModifiedDate(new Date());
    }

    public void updateChannel(UpdateCvsChannelRequest request) {
        this.password = request.getPassword() == null ? this.password : HashUtils.toSHA1(request.getPassword());
        this.segmentId = request.getSegmentId() == null ? this.segmentId : request.getSegmentId();
        this.username = request.getUsername() == null ? this.username : request.getUsername();
        this.campaignId = request.getCampaignId() == null ? this.campaignId : request.getCampaignId();
        this.setState(request.getState() == null ? this.getState() : request.getState());
        this.setLastModifiedDate(new Date());
    }

}
