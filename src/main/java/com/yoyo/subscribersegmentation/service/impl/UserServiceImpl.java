package com.yoyo.subscribersegmentation.service.impl;

import com.yoyo.subscribersegmentation.domain.enums.ConfigId;
import com.yoyo.subscribersegmentation.domain.enums.State;
import com.yoyo.subscribersegmentation.domain.models.*;
import com.yoyo.subscribersegmentation.exception.ApiException;
import com.yoyo.subscribersegmentation.exception.errors.ErrorCodes;
import com.yoyo.subscribersegmentation.handler.SEGMENTATIONLOGGER;
import com.yoyo.subscribersegmentation.repository.ConfigRepository;
import com.yoyo.subscribersegmentation.repository.SegmentRepository;
import com.yoyo.subscribersegmentation.repository.UserRepository;
import com.yoyo.subscribersegmentation.repository.UserSegmentRepository;
import com.yoyo.subscribersegmentation.domain.request.BulkDeleteAllUsersRequest;
import com.yoyo.subscribersegmentation.domain.request.BulkLoadUserRequest;
import com.yoyo.subscribersegmentation.domain.response.GetUsersResponse;
import com.yoyo.subscribersegmentation.domain.response.UserResponse;
import com.yoyo.subscribersegmentation.service.TenantService;
import com.yoyo.subscribersegmentation.service.UserService;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * @author anelechila
 */

@Service
@NoArgsConstructor
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private SegmentRepository segmentRepository;
    private UserSegmentRepository userSegmentRepository;
    private TenantService tenantService;
    private ConfigRepository configRepository;

    @Autowired
    public UserServiceImpl(TenantService tenantService, UserRepository userRepository, SegmentRepository segmentRepository,
                           UserSegmentRepository userSegmentRepository, ConfigRepository configRepository) {
        this.tenantService = tenantService;
        this.userRepository = userRepository;
        this.segmentRepository = segmentRepository;
        this.userSegmentRepository = userSegmentRepository;
        this.configRepository = configRepository;
    }


    @Override
    @SEGMENTATIONLOGGER
    public void bulkLoadUsers(BulkLoadUserRequest request) {
        Tenant tenant = tenantService.findById(request.getTenantId());

        List<String> userRefs = getFile(request.getFileName());

        userRefs.forEach(id -> {

            User user = userRepository.findByTenantUserId(id, tenant.getId());

            user = (user == null) ? userRepository.save(new User(id, tenant.getId())) : user;

            Segment segment = segmentRepository.findDistinctById(request.getSegmentId(), tenant.getId().intValue());

            if (segment == null || !segment.getState().equals(State.ACTIVE.getState())) {
                throw new ApiException(ErrorCodes.NOT_FOUND_SEGMENT.getResponseDesc());
            }

            UserSegment userSegment = new UserSegment();
            userSegment.create(user, segment);

            userSegmentRepository.save(userSegment);
        });
    }

    @Override
    @SEGMENTATIONLOGGER
    public void bulkReplaceUsers(BulkLoadUserRequest request) {
        Tenant tenant = tenantService.findById(request.getTenantId());

        Segment segment = segmentRepository.findDistinctById(request.getSegmentId(), tenant.getId().intValue());

        if (segment == null || !segment.getState().equals(State.ACTIVE.getState())) {
            throw new ApiException(ErrorCodes.NOT_FOUND_SEGMENT.getResponseDesc());
        }

        userSegmentRepository.findBySegmentId(segment.getId()).
                forEach( s -> { userSegmentRepository.delete(s);
                    userRepository.delete(s.getUser());
        });

        List<String> userRefs = getFile(request.getFileName());

        userRefs.forEach(id -> {

            User user = userRepository.findByTenantUserId(id, tenant.getId());

            user = (user == null) ? userRepository.save(new User(id, tenant.getId())) : user;

            UserSegment userSegment = new UserSegment();
            userSegment.create(user, segment);

            userSegmentRepository.save(userSegment);
        });
    }

    @Override
    @SEGMENTATIONLOGGER
    public void bulkDeleteUsers(BulkLoadUserRequest request) {
        Tenant tenant = tenantService.findById(request.getTenantId());

        Segment segment = segmentRepository.findDistinctById(request.getSegmentId(), tenant.getId().intValue());

        if (segment == null || !segment.getState().equals(State.ACTIVE.getState())) {
            throw new ApiException(ErrorCodes.NOT_FOUND_SEGMENT.getResponseDesc());
        }

        List<String> userRefs = getFile(request.getFileName());

        userRefs.forEach(id -> {
            User u = userRepository.findByTenantUserId(id, tenant.getId());
            userSegmentRepository.findByUserId(u.getId())
                    .stream().filter(us -> us.getId().getSegmentId().equals(segment.getId()))
                    .forEach(us -> { userSegmentRepository.delete(us); userRepository.delete(u);});
        });
    }

    @Override
    @SEGMENTATIONLOGGER
    public void bulkDeleteAllUsers(BulkDeleteAllUsersRequest request) {
        Tenant tenant = tenantService.findById(request.getTenantId());

        Segment segment = segmentRepository.findDistinctById(request.getSegmentId(), tenant.getId().intValue());

        if (segment == null || !segment.getState().equals(State.ACTIVE.getState())) {
            throw new ApiException(ErrorCodes.NOT_FOUND_SEGMENT.getResponseDesc());
        }

        userSegmentRepository.findBySegmentId(segment.getId()).
                forEach( s -> { userSegmentRepository.delete(s);
                    userRepository.delete(s.getUser());
                });
    }

    @Override
    @SEGMENTATIONLOGGER
    public GetUsersResponse getUsers(Long segmentId, Long tenantId) {

        Tenant tenant = tenantService.findById(tenantId);

        Segment segment = segmentRepository.findDistinctById(segmentId, tenant.getId().intValue());

        Function<String, ApiException> apiExceptionFunction = ApiException::new;

        if(segment == null)
          throw apiExceptionFunction.apply(ErrorCodes.NOT_FOUND_SEGMENT.getResponseDesc());

        List<UserResponse> response = new ArrayList<>();

        userSegmentRepository.findBySegmentId(segment.getId()).stream()
            .filter(userSegment -> userSegment.getState().equals(State.ACTIVE.getState()))
            .forEach(userSegment -> {
                User user = userRepository.findById(userSegment.getId().getUserId())
                .orElseThrow(() -> apiExceptionFunction.apply(ErrorCodes.NOT_FOUND_USER.getResponseDesc()));
                response.add(new UserResponse(user));
            });

        return new GetUsersResponse(response);
    }

    private List<String> getFile(String fileName) {
        try {
            //S3Object object = S3BucketUtil.downloadFromS3(fileName, findS3BucketName());
            //S3ObjectInputStream inputStream = object.getObjectContent();
            //return new InputStreamReaderUtil().readFromInputStream(inputStream);
            return null;
        } catch (Exception e) {
            throw new ApiException("Error downloading " + fileName + " from s3 bucket.");
        }
    }

    private String findS3BucketName() {
        Config config = configRepository.findById(ConfigId.TARGETED_MESSAGING_S3_BUCKET_NAME.name())
                .orElseThrow(() -> new ApiException(ErrorCodes.NOT_FOUND_CONFIG.getResponseDesc()));

        if (StringUtils.isBlank(config.getValue()) || "changeme".equals(config.getValue())) {
            throw new ApiException("CSV bucket name not configured");
        }

        return config.getValue();
    }
}
