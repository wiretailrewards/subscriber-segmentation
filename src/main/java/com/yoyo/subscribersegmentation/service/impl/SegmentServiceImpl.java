package com.yoyo.subscribersegmentation.service.impl;

import com.yoyo.subscribersegmentation.domain.enums.ConfigId;
import com.yoyo.subscribersegmentation.domain.enums.State;
import com.yoyo.subscribersegmentation.domain.models.Config;
import com.yoyo.subscribersegmentation.domain.models.Segment;
import com.yoyo.subscribersegmentation.domain.models.Tenant;
import com.yoyo.subscribersegmentation.domain.models.User;
import com.yoyo.subscribersegmentation.domain.request.CreateSegmentRequest;
import com.yoyo.subscribersegmentation.domain.request.UpdateSegmentRequest;
import com.yoyo.subscribersegmentation.domain.response.CampaignSegmentResponse;
import com.yoyo.subscribersegmentation.domain.response.CvsCampaignResponse;
import com.yoyo.subscribersegmentation.domain.response.GetSegmentsResponse;
import com.yoyo.subscribersegmentation.domain.response.SegmentResponse;
import com.yoyo.subscribersegmentation.exception.ApiException;
import com.yoyo.subscribersegmentation.exception.errors.ErrorCodes;
import com.yoyo.subscribersegmentation.handler.SEGMENTATIONLOGGER;
import com.yoyo.subscribersegmentation.repository.*;
import com.yoyo.subscribersegmentation.service.SegmentService;
import com.yoyo.subscribersegmentation.service.TenantService;
import com.yoyo.subscribersegmentation.utils.S3BucketUtil;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Function;

/**
 * @author anelechila
 */

@Service
@NoArgsConstructor
public class SegmentServiceImpl implements SegmentService {

    private SegmentRepository segmentRepository;
    private CvsCampaignRepository cvsCampaignRepository;
    private TenantService tenantService;
    private UserRepository userRepository;
    private UserSegmentRepository userSegmentRepository;

    @Autowired
    public SegmentServiceImpl(SegmentRepository segmentRepository, CvsCampaignRepository cvsCampaignRepository,
                              UserRepository userRepository, UserSegmentRepository userSegmentRepository,
                              TenantService tenantService) {
        this.segmentRepository = segmentRepository;
        this.cvsCampaignRepository = cvsCampaignRepository;
        this.userRepository = userRepository;
        this.userSegmentRepository = userSegmentRepository;
        this.tenantService = tenantService;
    }

    @Override
    @SEGMENTATIONLOGGER
    public List<SegmentResponse> findAllSegments() {

        List<Segment> segments = getSegments();

        List<SegmentResponse> segmentResponses = new ArrayList<>();
        segments.stream()
                .filter(Objects::nonNull)
                .forEach(s ->  segmentResponses.add(new SegmentResponse(s)));


        return segmentResponses;
    }

    private List<Segment> getSegments() {
        List<Segment> segment;
        try {
            segment = findAll();

        } catch (Exception e) {
            throw new ApiException(ErrorCodes.GENERAL_DATABASE_ERR.getResponseDesc());
        }
        return segment;
    }

    @Override
    @SEGMENTATIONLOGGER
    public SegmentResponse findSegmentsById(Long id) {

        try {
            Segment segment = findById(id);

            if(segment == null) {
                throw new ApiException(ErrorCodes.NOT_FOUND_SEGMENT.getResponseDesc());
            }

            SegmentResponse response = new SegmentResponse(segment);
            response.setCount(userSegmentRepository.findBySegmentId(segment.getId()).size());

            return response;
        } catch (Exception e) {
            throw new ApiException(ErrorCodes.GENERAL_DATABASE_ERR.getResponseDesc());
        }
    }

    @Override
    @SEGMENTATIONLOGGER
    public SegmentResponse createSegment(CreateSegmentRequest request) {
        try {
            tenantService.findById(request.getTenantId());
            Segment segment = new Segment();
            segment.createSegment(request);
            Segment segmentDb = save(segment);

            return new SegmentResponse(segmentDb);
        } catch (Exception e) {
            throw new ApiException(ErrorCodes.GENERAL_DATABASE_ERR.getResponseDesc());
        }
    }

    @Override
    @SEGMENTATIONLOGGER
    public SegmentResponse updateSegment(UpdateSegmentRequest request) {
        Segment segment = findById(request.getId());

        if (request.getTenantId() != null) {
            tenantService.findById(request.getTenantId().longValue());
        }

        segment.updateSegment(request);
        Segment segmentDb = save(segment);

        return new SegmentResponse(segmentDb);
    }

    @Override
    @SEGMENTATIONLOGGER
    public void delete(Segment segment){
        try {
            segmentRepository.delete(segment);
        } catch (Exception e) {
            throw new ApiException(ErrorCodes.GENERAL_DATABASE_ERR.getResponseDesc());
        }
    }

    @Override
    @SEGMENTATIONLOGGER
    public void deleteById(Long id){
        try {
            Segment segment = findById(id);
            delete(segment);
        } catch (Exception e) {
            throw new ApiException(ErrorCodes.GENERAL_DATABASE_ERR.getResponseDesc());
        }
    }

    @Override
    public List<Segment> findAll() {
        return segmentRepository.findAll();
    }
    @Override
    public Segment findById(Long id) {
        Optional<Segment> optionalSegment = segmentRepository.findById(id);

        return optionalSegment.orElseThrow(() -> new ApiException(ErrorCodes.NOT_FOUND_SEGMENT.getResponseDesc()));
    }

    @Override
    public Segment save(Segment segment) {

        return segmentRepository.save(segment);
    }

    @Override
    @SEGMENTATIONLOGGER
    public GetSegmentsResponse getSegmentsByUserId(String userRef, Long tenantId) {

        GetSegmentsResponse userResponse = new GetSegmentsResponse();
        CampaignSegmentResponse campaignSegmentResponse = new CampaignSegmentResponse();

        Function<String, ApiException> apiExceptionFunction = ApiException::new;

        User user = userRepository.findByTenantUserId(userRef, tenantId);

        if(user == null) {
            throw new ApiException(ErrorCodes.NOT_FOUND_USER.getResponseDesc());
        }

        userSegmentRepository.findByUserId(user.getId()).stream()
                .filter(userSegment -> userSegment.getState().equals(State.ACTIVE.getState()))
                .map(userSegment -> segmentRepository.findById(userSegment.getId().getSegmentId())
                        .orElseThrow(() -> apiExceptionFunction.apply(ErrorCodes.NOT_FOUND_SEGMENT.getResponseDesc())))
                .forEach(segment -> addSegmentsToGetResponse(userResponse, campaignSegmentResponse, segment));

        return userResponse;
    }

    @Override
    @SEGMENTATIONLOGGER
    public GetSegmentsResponse getSegmentsByTenantId(Long tenantId) {

        GetSegmentsResponse userResponse = new GetSegmentsResponse();
        CampaignSegmentResponse campaignSegmentResponse = new CampaignSegmentResponse();

        Tenant tenant = tenantService.findById(tenantId);

        segmentRepository.findByTenantId(tenant.getId().intValue()).
                forEach(segment -> addSegmentsToGetResponse(userResponse, campaignSegmentResponse, segment));

        return userResponse;
    }

    @SEGMENTATIONLOGGER
    private void addSegmentsToGetResponse(GetSegmentsResponse userResponse, CampaignSegmentResponse campaignSegmentResponse, Segment segment) {
        campaignSegmentResponse.setSegment(new SegmentResponse(segment));

        List<CvsCampaignResponse> cvsChannels = new ArrayList<>();

        cvsCampaignRepository.findBySegmentId(segment.getId()).
                forEach(cvsChannel -> {
                    CvsCampaignResponse cvsCampaignResponse = new CvsCampaignResponse(cvsChannel);
                    cvsChannels.add(cvsCampaignResponse);
                });

        campaignSegmentResponse.setCvsCampaigns(cvsChannels);

        userResponse.getCampaignSegments().add(campaignSegmentResponse);
    }

}
