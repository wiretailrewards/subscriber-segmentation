package com.yoyo.subscribersegmentation.service.impl;

import com.yoyo.subscribersegmentation.domain.models.CvsCampaign;
import com.yoyo.subscribersegmentation.exception.ApiException;
import com.yoyo.subscribersegmentation.exception.errors.ErrorCodes;
import com.yoyo.subscribersegmentation.handler.SEGMENTATIONLOGGER;
import com.yoyo.subscribersegmentation.repository.CvsCampaignRepository;
import com.yoyo.subscribersegmentation.repository.SegmentRepository;
import com.yoyo.subscribersegmentation.domain.request.CreateCvsChannelRequest;
import com.yoyo.subscribersegmentation.domain.request.UpdateCvsChannelRequest;
import com.yoyo.subscribersegmentation.domain.response.CvsCampaignResponse;
import com.yoyo.subscribersegmentation.service.CvsCampaignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author anelechila
 */

@Service
public class CvsCampaignServiceImpl implements CvsCampaignService {

    private final CvsCampaignRepository cvsCampaignRepository;
    private final SegmentRepository segmentRepository;

    @Autowired
    public CvsCampaignServiceImpl(SegmentRepository segmentRepository, CvsCampaignRepository cvsCampaignRepository) {
        this.cvsCampaignRepository = cvsCampaignRepository;
        this.segmentRepository = segmentRepository;
    }

    @Override
    @SEGMENTATIONLOGGER
    public List<CvsCampaignResponse> findAllChannels() {

        List<CvsCampaign> cvsCampaigns = getChannels();

        List<CvsCampaignResponse> cvsCampaignRespons = new ArrayList<>();
        cvsCampaigns.stream()
                .filter(Objects::nonNull)
                .forEach(s ->  cvsCampaignRespons.add(new CvsCampaignResponse(s)));


        return cvsCampaignRespons;
    }

    private List<CvsCampaign> getChannels() {
        List<CvsCampaign> cvsCampaigns;
        try {
            cvsCampaigns = findAll();

        } catch (Exception e) {
            throw new ApiException(ErrorCodes.GENERAL_DATABASE_ERR.getResponseDesc());
        }
        return cvsCampaigns;
    }

    @Override
    @SEGMENTATIONLOGGER
    public CvsCampaignResponse findChannelsById(Long id) {

        try {
            CvsCampaign cvsCampaign = findById(id);

            if(cvsCampaign == null) {
                throw new ApiException(ErrorCodes.NOT_FOUND_CVS_CHANNEL.getResponseDesc());
            }

            return new CvsCampaignResponse(cvsCampaign);
        } catch (Exception e) {
            throw new ApiException(ErrorCodes.GENERAL_DATABASE_ERR.getResponseDesc());
        }
    }

    @Override
    @SEGMENTATIONLOGGER
    public CvsCampaignResponse createChannel(CreateCvsChannelRequest request) {

        try {
            CvsCampaign cvsCampaign = new CvsCampaign();
            segmentRepository.findById(request.getSegmentId())
                    .orElseThrow(() -> new  ApiException(ErrorCodes.NOT_FOUND_SEGMENT.getResponseDesc()));

            cvsCampaign.createChannel(request);
            CvsCampaign channelDb = save(cvsCampaign);

            return new CvsCampaignResponse(channelDb);
        } catch (Exception e) {
            throw new ApiException(ErrorCodes.GENERAL_DATABASE_ERR.getResponseDesc());
        }
    }

    @Override
    @SEGMENTATIONLOGGER
    public CvsCampaignResponse updateChannel(UpdateCvsChannelRequest request) {
        CvsCampaign cvsCampaign = findById(request.getId());

        if(request.getSegmentId() != null) {
            segmentRepository.findById(request.getSegmentId())
            .orElseThrow(() -> new  ApiException(ErrorCodes.NOT_FOUND_SEGMENT.getResponseDesc()));
        }

        cvsCampaign.updateChannel(request);
        CvsCampaign channelDb = save(cvsCampaign);

        return new CvsCampaignResponse(channelDb);

    }

    @Override
    @SEGMENTATIONLOGGER
    public void delete(CvsCampaign cvsCampaign){
        try {
            cvsCampaignRepository.delete(cvsCampaign);
        } catch (Exception e) {
            throw new ApiException(ErrorCodes.GENERAL_DATABASE_ERR.getResponseDesc());
        }
    }

    @Override
    @SEGMENTATIONLOGGER
    public void deleteById(Long id){
        try {

            CvsCampaign cvsCampaign = findById(id);
            delete(cvsCampaign);
        } catch (Exception e) {
            throw new ApiException(ErrorCodes.GENERAL_DATABASE_ERR.getResponseDesc());
        }
    }

    @Override
    public List<CvsCampaign> findAll() {
        return cvsCampaignRepository.findAll();
    }
    @Override
    public CvsCampaign findById(Long id) {
        Optional<CvsCampaign> cvsChannel = cvsCampaignRepository.findById(id);

        return cvsChannel.orElseThrow(() -> new ApiException(ErrorCodes.NOT_FOUND_CVS_CHANNEL.getResponseDesc()));
    }

    @Override
    public CvsCampaign save(CvsCampaign cvsCampaign) {

        return cvsCampaignRepository.save(cvsCampaign);
    }

}
