package com.yoyo.subscribersegmentation.service.impl;

import com.yoyo.subscribersegmentation.handler.SEGMENTATIONLOGGER;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@Component
public class RestService {

    @SEGMENTATIONLOGGER
    public boolean validateJWT(String url, String jwt) {
        RestTemplate restTemplate = new RestTemplate();
        try {
            ResponseEntity<Object> response = restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(createHeaders(jwt)), Object.class);
            return response.getStatusCode() == HttpStatus.OK;
        } catch (Exception e) {
            return false;
        }
    }

    private HttpHeaders createHeaders(String jwt) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set( "Authorization", "JWT " + jwt );
        return headers;
    }
}
