package com.yoyo.subscribersegmentation.service.impl;

import com.yoyo.subscribersegmentation.domain.enums.State;
import com.yoyo.subscribersegmentation.domain.models.Tenant;
import com.yoyo.subscribersegmentation.exception.ApiException;
import com.yoyo.subscribersegmentation.exception.AuthenticationException;
import com.yoyo.subscribersegmentation.exception.errors.ErrorCodes;
import com.yoyo.subscribersegmentation.handler.SEGMENTATIONLOGGER;
import com.yoyo.subscribersegmentation.repository.TenantRepository;
import com.yoyo.subscribersegmentation.domain.request.CreateTenantRequest;
import com.yoyo.subscribersegmentation.domain.request.UpdateTenantRequest;
import com.yoyo.subscribersegmentation.domain.response.TenantResponse;
import com.yoyo.subscribersegmentation.service.TenantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author anelechila
 */

@Service
public class TenantServiceImpl implements TenantService {

    private final TenantRepository tenantRepository;

    @Autowired
    public TenantServiceImpl(TenantRepository tenantRepository) {
        this.tenantRepository = tenantRepository;
    }

    @Override
    @SEGMENTATIONLOGGER
    public List<TenantResponse> findAllTenants() {

        List<Tenant> tenants = getSegments();

        List<TenantResponse> tenantResponses = new ArrayList<>();
        tenants.stream()
                .filter(Objects::nonNull)
                .forEach(s ->  tenantResponses.add(new TenantResponse(s)));


        return tenantResponses;
    }

    private List<Tenant> getSegments() {
        List<Tenant> tenants;
        try {
            tenants = findAll();

        } catch (Exception e) {
            throw new ApiException(ErrorCodes.GENERAL_DATABASE_ERR.getResponseDesc());
        }
        return tenants;
    }

    @Override
    @SEGMENTATIONLOGGER
    public TenantResponse findTenantsById(Long id) {
        Tenant tenant = findById(id);
        return new TenantResponse(tenant);
    }

    @Override
    @SEGMENTATIONLOGGER
    public TenantResponse createTenant(CreateTenantRequest request) {
        Tenant tenant = new Tenant();
        tenant.createTenant(request);
        Tenant tenantDb = save(tenant);

        return new TenantResponse(tenantDb);
    }

    @Override
    @SEGMENTATIONLOGGER
    public TenantResponse updateTenant(UpdateTenantRequest request) {
        Tenant tenant = findById(request.getId());
        tenant.updateTenant(request);
        Tenant tenantDb = save(tenant);

        return new TenantResponse(tenantDb);
    }

    @Override
    public List<Tenant> findAll() {
        return tenantRepository.findAll();
    }

    @Override
    public Tenant findById(Long id) {
        return tenantRepository.findById(id).orElseThrow(() -> new ApiException(ErrorCodes.NOT_FOUND_TENANT.getResponseDesc()));
    }

    @Override
    public Tenant save(Tenant object) {
        try {
            return tenantRepository.save(object);
        } catch (DataIntegrityViolationException e) {
            throw new ApiException(e.getRootCause().getMessage());
        }
    }

    @Override
    @SEGMENTATIONLOGGER
    public void delete(Tenant tenant){
        try {
            tenantRepository.delete(tenant);
        } catch (Exception e) {
            throw new ApiException(ErrorCodes.GENERAL_DATABASE_ERR.getResponseDesc());
        }
    }

    @Override
    @SEGMENTATIONLOGGER
    public void deleteById(Long id) {
        Tenant tenant = findById(id);
        tenant.setState(State.DELETED.getState());
        tenant.setLastModifiedDate(new Date());

        save(tenant);
    }

    @Override
    @SEGMENTATIONLOGGER
    public TenantResponse getTenantByCredentials(String tenantName, String tenantPassword) {
        Tenant tenant = tenantRepository.getTenantByCredentials(tenantName, tenantPassword);

        if (tenant == null || !tenant.getState().equals(State.ACTIVE.getState())) {
            throw new AuthenticationException(ErrorCodes.AUTHENTICATION_FAILED.getResponseDesc());
        }

        return new TenantResponse(tenant);
    }
}
