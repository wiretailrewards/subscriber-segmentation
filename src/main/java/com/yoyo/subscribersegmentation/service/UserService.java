package com.yoyo.subscribersegmentation.service;

import com.yoyo.subscribersegmentation.domain.request.BulkDeleteAllUsersRequest;
import com.yoyo.subscribersegmentation.domain.request.BulkDeleteUserRequest;
import com.yoyo.subscribersegmentation.domain.request.BulkLoadUserRequest;
import com.yoyo.subscribersegmentation.domain.response.GetUsersResponse;

public interface UserService {

    void bulkLoadUsers(BulkLoadUserRequest request);
    void bulkReplaceUsers(BulkLoadUserRequest request);
    void bulkDeleteUsers(BulkLoadUserRequest request);
    void bulkDeleteAllUsers(BulkDeleteAllUsersRequest request);
    GetUsersResponse getUsers(Long segmentId, Long tenantId);
}
