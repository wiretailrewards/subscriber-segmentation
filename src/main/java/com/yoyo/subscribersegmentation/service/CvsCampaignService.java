package com.yoyo.subscribersegmentation.service;

import com.yoyo.subscribersegmentation.domain.models.CvsCampaign;
import com.yoyo.subscribersegmentation.domain.request.CreateCvsChannelRequest;
import com.yoyo.subscribersegmentation.domain.request.UpdateCvsChannelRequest;
import com.yoyo.subscribersegmentation.domain.response.CvsCampaignResponse;

import java.util.List;

public interface CvsCampaignService extends CrudService<CvsCampaign, Long> {
    List<CvsCampaignResponse> findAllChannels();
    CvsCampaignResponse findChannelsById(Long id);
    CvsCampaignResponse createChannel(CreateCvsChannelRequest request);
    CvsCampaignResponse updateChannel(UpdateCvsChannelRequest request);
}
