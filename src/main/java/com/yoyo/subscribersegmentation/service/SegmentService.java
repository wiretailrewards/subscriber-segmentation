package com.yoyo.subscribersegmentation.service;

import com.yoyo.subscribersegmentation.domain.models.Segment;
import com.yoyo.subscribersegmentation.domain.request.CreateSegmentRequest;
import com.yoyo.subscribersegmentation.domain.request.UpdateSegmentRequest;
import com.yoyo.subscribersegmentation.domain.response.GetSegmentsResponse;
import com.yoyo.subscribersegmentation.domain.response.SegmentResponse;

import java.util.List;

/**
 * @author anelechila
 */
public interface SegmentService extends CrudService<Segment, Long> {
    List<SegmentResponse> findAllSegments();
    SegmentResponse findSegmentsById(Long id);
    SegmentResponse createSegment(CreateSegmentRequest segment);
    SegmentResponse updateSegment(UpdateSegmentRequest request);
    GetSegmentsResponse getSegmentsByUserId(String userRef, Long tenantId);

    GetSegmentsResponse getSegmentsByTenantId(Long tenantId);
}
