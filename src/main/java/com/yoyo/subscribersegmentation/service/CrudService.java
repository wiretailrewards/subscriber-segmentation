package com.yoyo.subscribersegmentation.service;

import java.util.List;

/**
 * @author anelechila
 */
public interface CrudService<T, ID> {

    List<T> findAll();

    T findById(ID id);

    T save(T object);

    void delete(T object);

    void deleteById(ID id);

}
