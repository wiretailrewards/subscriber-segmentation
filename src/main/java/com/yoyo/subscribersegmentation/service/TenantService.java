package com.yoyo.subscribersegmentation.service;

import com.yoyo.subscribersegmentation.domain.models.Tenant;
import com.yoyo.subscribersegmentation.domain.request.CreateTenantRequest;
import com.yoyo.subscribersegmentation.domain.request.UpdateTenantRequest;
import com.yoyo.subscribersegmentation.domain.response.TenantResponse;

import java.util.List;

/**
 * @author anelechila
 */
public interface TenantService extends CrudService<Tenant, Long> {

    List<TenantResponse> findAllTenants();
    TenantResponse findTenantsById(Long id);
    TenantResponse createTenant(CreateTenantRequest segment);
    TenantResponse updateTenant(UpdateTenantRequest request);
    TenantResponse getTenantByCredentials(String tenantName, String tenantPassword);
}
