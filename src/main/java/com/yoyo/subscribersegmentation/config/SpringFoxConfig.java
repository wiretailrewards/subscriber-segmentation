package com.yoyo.subscribersegmentation.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;

@Configuration
public class SpringFoxConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.yoyo.subscribersegmentation.controller"))
                .paths(PathSelectors.any()).build().host("localhost:8080").pathMapping("/").apiInfo(metaData());
    }

    private ApiInfo metaData() {
        Contact contact = new Contact("Anele Chila", null, "anele.chila@yoyowallet.com");

        return new ApiInfo("Subscriber Segmentation API", "Subscriber Segmentation", "1.0", "", contact,
                "Private", "", new ArrayList<>());
    }
}
