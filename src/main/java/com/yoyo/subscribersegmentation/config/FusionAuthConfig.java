package com.yoyo.subscribersegmentation.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:fusionauth.properties")
@ConfigurationProperties("fusion-auth")
@Data
public class FusionAuthConfig {

    private String url;
}