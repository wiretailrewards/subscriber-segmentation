package com.yoyo.subscribersegmentation.handler;

import com.yoyo.subscribersegmentation.exception.ApiException;
import com.yoyo.subscribersegmentation.exception.AuthenticationException;
import com.yoyo.subscribersegmentation.exception.InvalidRequestException;
import com.yoyo.subscribersegmentation.exception.errors.ErrorCodes;
import com.yoyo.subscribersegmentation.exception.errors.ErrorField;
import com.yoyo.subscribersegmentation.exception.errors.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;

/**
 * @author anelechila
 */
@ControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {
	private final Logger logger = LoggerFactory.getLogger(getClass());

    @ExceptionHandler({InvalidRequestException.class})
    protected ResponseEntity<Object> handleInvalidRequest(InvalidRequestException ire) {
      logger.info("============================== InvalidRequestException caught", ire);

      ErrorResponse errorResponse = new ErrorResponse(ErrorCodes.INVALID_REQUEST.getResponseCode(), ire.getMessage());
      errorResponse.addGlobalError(ire.getMessage());

      List<FieldError> fieldErrors = ire.getErrors().getFieldErrors();
      for (FieldError fieldError : fieldErrors) {
          ErrorField errorField = new ErrorField();
          errorField.setCode(fieldError.getCode());
          errorField.setMessage(ErrorCodes.FIELD_VALIDATION_ERR.getResponseDesc() + fieldError.getField());
          errorResponse.addError(errorField);
      }

      return new ResponseEntity<>(errorResponse, ErrorCodes.INVALID_REQUEST.getHttpStatus());
    }

    @ExceptionHandler({AuthenticationException.class})
    protected ResponseEntity<Object> handleAuthenticationException(AuthenticationException authExc) {
        logger.info("============================== AuthenticationException caught ============================== \n", authExc);

        ErrorResponse error = new ErrorResponse(ErrorCodes.AUTHENTICATION_FAILED.getResponseCode(), authExc.getMessage());
        error.addGlobalError(authExc.getMessage());

        return new ResponseEntity<>(error, ErrorCodes.INVALID_REQUEST.getHttpStatus());
    }


    @ExceptionHandler({ApiException.class})
    protected ResponseEntity<Object> handleBadRequest(ApiException bre) {
        logger.info("============================== BadRequestException caught ============================== \n", bre);

        ErrorResponse error = new ErrorResponse(ErrorCodes.BAD_REQUEST.getResponseCode(), bre.getMessage());
        error.addGlobalError(bre.getMessage());

        return new ResponseEntity<>(error, ErrorCodes.INVALID_REQUEST.getHttpStatus());
    }

    @ExceptionHandler(value = { Exception.class, RuntimeException.class })
    protected ResponseEntity<Object> defaultExceptionHandler(Exception e) {
        logger.info("============================== Exception caught ============================== \n", e);

        ErrorResponse error = new ErrorResponse(ErrorCodes.GENERAL_SYSTEM_ERR.getResponseDesc(),getRootCause(e).getMessage());
        error.addGlobalError(e.getMessage());

        return new ResponseEntity<>(error, ErrorCodes.INVALID_REQUEST.getHttpStatus());
    }

    public Throwable getRootCause(Throwable original) {
        if (original == null) {
            return null;
        }
        Throwable rootCause = null;
        Throwable cause = original.getCause();
        while (cause != null && cause != rootCause) {
            rootCause = cause;
            cause = cause.getCause();
        }
        return rootCause;
    }
}
