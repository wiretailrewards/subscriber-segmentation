package com.yoyo.subscribersegmentation.handler;


import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface SEGMENTATIONLOGGER {

}