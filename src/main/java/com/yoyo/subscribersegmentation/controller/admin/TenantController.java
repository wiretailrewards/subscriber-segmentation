package com.yoyo.subscribersegmentation.controller.admin;

import com.yoyo.subscribersegmentation.exception.InvalidRequestException;
import com.yoyo.subscribersegmentation.domain.request.CreateTenantRequest;
import com.yoyo.subscribersegmentation.domain.request.UpdateTenantRequest;
import com.yoyo.subscribersegmentation.domain.response.GetSegmentsResponse;
import com.yoyo.subscribersegmentation.domain.response.TenantResponse;
import com.yoyo.subscribersegmentation.service.SegmentService;
import com.yoyo.subscribersegmentation.service.TenantService;
import com.yoyo.subscribersegmentation.validator.TenantValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.yoyo.subscribersegmentation.exception.errors.ErrorCodes.INVALID_REQUEST;

/**
 * @author anelechila
 */
@RestController
@RequestMapping("/admin/tenants")
public class TenantController {

    private final TenantValidator tenantValidator;
    private final SegmentService segmentService;
    private final TenantService tenantService;

    @Autowired
    public TenantController(SegmentService segmentService, TenantService tenantService, TenantValidator tenantValidator) {
        this.segmentService = segmentService;
        this.tenantService = tenantService;
        this.tenantValidator = tenantValidator;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public TenantResponse createTenant(@RequestHeader("apiKey") String apiKey, @RequestBody CreateTenantRequest request,
                                        BindingResult bindingResult) {

        tenantValidator.validate(request, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new InvalidRequestException(INVALID_REQUEST.getResponseDesc(), bindingResult);
        }

        return tenantService.createTenant(request);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public TenantResponse getTenant(@RequestHeader("apiKey") String apiKey, @PathVariable("id") Long id) {
        return tenantService.findTenantsById(id);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<TenantResponse> getAllTenants(@RequestHeader("apiKey") String apiKey) {
        return tenantService.findAllTenants();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public TenantResponse updateTenant(@RequestHeader("apiKey") String apiKey,
                                       @RequestBody UpdateTenantRequest request, @PathVariable("id") Long id,
                                         BindingResult bindingResult) {

        tenantValidator.validateForUpdate(request, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new InvalidRequestException(INVALID_REQUEST.getResponseDesc(), bindingResult);
        }

        request.setId(id);
        return tenantService.updateTenant(request);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Boolean deleteTenant(@RequestHeader("apiKey") String apiKey, @PathVariable("id") Long id) {
        tenantService.deleteById(id);
        return true;
    }

    @RequestMapping(value = "/{id}/segments", method = RequestMethod.GET)
    public GetSegmentsResponse getSegmentsByTenantId(@RequestHeader("apiKey") String apiKey, @PathVariable("id") Long id) {
        return segmentService.getSegmentsByTenantId(id);
    }
}
