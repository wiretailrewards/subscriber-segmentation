package com.yoyo.subscribersegmentation.controller.admin;

import com.yoyo.subscribersegmentation.exception.InvalidRequestException;
import com.yoyo.subscribersegmentation.domain.request.BulkDeleteAllUsersRequest;
import com.yoyo.subscribersegmentation.domain.request.BulkDeleteUserRequest;
import com.yoyo.subscribersegmentation.domain.request.BulkLoadUserRequest;
import com.yoyo.subscribersegmentation.domain.response.GetSegmentsResponse;
import com.yoyo.subscribersegmentation.domain.response.GetUsersResponse;
import com.yoyo.subscribersegmentation.service.SegmentService;
import com.yoyo.subscribersegmentation.service.UserService;
import com.yoyo.subscribersegmentation.validator.UserValidator;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import static com.yoyo.subscribersegmentation.exception.errors.ErrorCodes.INVALID_REQUEST;

/**
 * @author anelechila
 */
@RestController
@RequestMapping("/admin/users")
@NoArgsConstructor
public class UserController {

    private UserService userService;
    private SegmentService segmentService;
    private UserValidator userValidator;

    @Autowired
    public UserController(SegmentService segmentService, UserService userService, UserValidator userValidator) {
        this.segmentService = segmentService;
        this.userService = userService;
        this.userValidator = userValidator;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public void uploadUsers(@RequestHeader("apiKey") String apiKey, @RequestBody BulkLoadUserRequest request, BindingResult bindingResult) {

        userValidator.validate(request, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new InvalidRequestException(INVALID_REQUEST.getResponseDesc(), bindingResult);
        }

        userService.bulkLoadUsers(request);
    }

    @RequestMapping(value = "/replace", method = RequestMethod.POST)
    public void replaceUsers(@RequestHeader("apiKey") String apiKey, @RequestBody BulkLoadUserRequest request, BindingResult bindingResult) {

        userValidator.validate(request, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new InvalidRequestException(INVALID_REQUEST.getResponseDesc(), bindingResult);
        }

        userService.bulkReplaceUsers(request);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public void deleteUsers(@RequestHeader("apiKey") String apiKey, @RequestBody BulkLoadUserRequest request, BindingResult bindingResult) {

        userValidator.validate(request, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new InvalidRequestException(INVALID_REQUEST.getResponseDesc(), bindingResult);
        }

        userService.bulkDeleteUsers(request);
    }

    @RequestMapping(value = "", method = RequestMethod.DELETE)
    public void deleteAllUsers(@RequestHeader("apiKey") String apiKey, @RequestBody BulkDeleteAllUsersRequest request, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            throw new InvalidRequestException(INVALID_REQUEST.getResponseDesc(), bindingResult);
        }

        userService.bulkDeleteAllUsers(request);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public GetUsersResponse getUsers(@RequestHeader("apiKey") String apiKey,
                                     @RequestParam("segmentId") Long segmentId, @RequestParam("tenantId") Long tenantId) {
        return userService.getUsers(segmentId,tenantId);
    }

    @RequestMapping(value = "/segments", method = RequestMethod.GET)
    public GetSegmentsResponse getSegments(@RequestHeader("apiKey") String apiKey, @RequestParam("userRef") String userRef, @RequestParam("tenantId") Long tenantId) {
        return segmentService.getSegmentsByUserId(userRef, tenantId);
    }
}
