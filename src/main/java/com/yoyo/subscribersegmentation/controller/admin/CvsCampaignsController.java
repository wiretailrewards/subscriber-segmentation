package com.yoyo.subscribersegmentation.controller.admin;

import com.yoyo.subscribersegmentation.exception.InvalidRequestException;
import com.yoyo.subscribersegmentation.domain.request.CreateCvsChannelRequest;
import com.yoyo.subscribersegmentation.domain.request.UpdateCvsChannelRequest;
import com.yoyo.subscribersegmentation.domain.response.CvsCampaignResponse;
import com.yoyo.subscribersegmentation.service.CvsCampaignService;
import com.yoyo.subscribersegmentation.validator.CvsChannelValidator;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.yoyo.subscribersegmentation.exception.errors.ErrorCodes.INVALID_REQUEST;

/**
 * @author anelechila
 */

@RestController
@RequestMapping("/admin/cvscampaigns")
public class CvsCampaignsController {

    private final CvsCampaignService cvsCampaignService;
    private final CvsChannelValidator cvsChannelValidator;

    private CvsCampaignsController(CvsCampaignService cvsCampaignService, CvsChannelValidator cvsChannelValidator) {
        this.cvsCampaignService = cvsCampaignService;
        this.cvsChannelValidator = cvsChannelValidator;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public CvsCampaignResponse createChannel(@RequestBody CreateCvsChannelRequest request,
                                             @RequestHeader("apiKey") String apiKey,
                                             BindingResult bindingResult) {

        cvsChannelValidator.validate(request, bindingResult);
        if(bindingResult.hasErrors()) {
            throw new InvalidRequestException(INVALID_REQUEST.getResponseDesc(), bindingResult);
        }

        return cvsCampaignService.createChannel(request);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public CvsCampaignResponse getCvsChannel(@RequestHeader("apiKey") String apiKey, @PathVariable("id") Long id) {
        return cvsCampaignService.findChannelsById(id);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<CvsCampaignResponse> getAllChannels(@RequestHeader("apiKey") String apiKey) {
        return cvsCampaignService.findAllChannels();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public CvsCampaignResponse updateChannel(@RequestHeader("apiKey") String apiKey,
                                             @RequestBody UpdateCvsChannelRequest request, @PathVariable("id") Long id,
                                             BindingResult bindingResult) {

        cvsChannelValidator.validateForUpdate(request, bindingResult);
        if(bindingResult.hasErrors()) {
            throw new InvalidRequestException(INVALID_REQUEST.getResponseDesc(), bindingResult);
        }

        request.setId(id);
        return cvsCampaignService.updateChannel(request);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public boolean deleteChannel(@RequestHeader("apiKey") String apiKey, @PathVariable("id") Long id) {
        cvsCampaignService.deleteById(id);
        return true;
    }
}
