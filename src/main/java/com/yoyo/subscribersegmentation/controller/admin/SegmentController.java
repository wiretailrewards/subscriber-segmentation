package com.yoyo.subscribersegmentation.controller.admin;

import com.yoyo.subscribersegmentation.exception.InvalidRequestException;
import com.yoyo.subscribersegmentation.domain.request.CreateSegmentRequest;
import com.yoyo.subscribersegmentation.domain.request.UpdateSegmentRequest;
import com.yoyo.subscribersegmentation.domain.response.SegmentResponse;
import com.yoyo.subscribersegmentation.service.SegmentService;
import com.yoyo.subscribersegmentation.validator.SegmentValidator;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import static com.yoyo.subscribersegmentation.exception.errors.ErrorCodes.INVALID_REQUEST;

/**
 * @author anelechila
 */
@RestController
@RequestMapping("/admin/segments")
public class SegmentController {

    private final SegmentService segmentService;
    private final SegmentValidator segmentValidator;


    private SegmentController(SegmentService segmentService, SegmentValidator segmentValidator) {
        this.segmentService = segmentService;
        this.segmentValidator = segmentValidator;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public SegmentResponse createSegment(@RequestHeader("apiKey") String apiKey,
                                         @RequestBody CreateSegmentRequest request,
                                         BindingResult bindingResult) {

        segmentValidator.validate(request, bindingResult);
        if(bindingResult.hasErrors()) {
            throw new InvalidRequestException(INVALID_REQUEST.getResponseDesc(), bindingResult);
        }

        return segmentService.createSegment(request);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public SegmentResponse getSegment(@RequestHeader("apiKey") String apiKey,
                                      @PathVariable("id") Long id) {
        return segmentService.findSegmentsById(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public SegmentResponse updateSegment(@RequestHeader("apiKey") String apiKey,
                                         @RequestBody UpdateSegmentRequest request, @PathVariable("id") Long id,
                                         BindingResult bindingResult) {

        segmentValidator.validateForUpdate(request, bindingResult);
        if(bindingResult.hasErrors()) {
            throw new InvalidRequestException(INVALID_REQUEST.getResponseDesc(), bindingResult);
        }

        request.setId(id);
        return segmentService.updateSegment(request);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public boolean deleteSegment(@RequestHeader("apiKey") String apiKey, @PathVariable("id") Long id) {
        segmentService.deleteById(id);
        return true;
    }
}
