package com.yoyo.subscribersegmentation.controller.app;

import com.yoyo.subscribersegmentation.exception.InvalidRequestException;
import com.yoyo.subscribersegmentation.domain.request.UpdateSegmentRequest;
import com.yoyo.subscribersegmentation.domain.response.GetSegmentsResponse;
import com.yoyo.subscribersegmentation.domain.response.SegmentResponse;
import com.yoyo.subscribersegmentation.service.SegmentService;
import com.yoyo.subscribersegmentation.validator.SegmentValidator;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import static com.yoyo.subscribersegmentation.exception.errors.ErrorCodes.INVALID_REQUEST;

/**
 * @author anelechila
 */
@RestController
@RequestMapping("/segments")
public class AppSegmentController {

    private final SegmentService segmentService;
    private final SegmentValidator segmentValidator;


    private AppSegmentController(SegmentService segmentService, SegmentValidator segmentValidator) {
        this.segmentService = segmentService;
        this.segmentValidator = segmentValidator;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public SegmentResponse updateSegment(@RequestHeader("name") String name, @RequestHeader("password") String password,
                                         @RequestBody UpdateSegmentRequest request, @PathVariable("id") Long id,
                                         BindingResult bindingResult) {

        segmentValidator.validateForUpdate(request, bindingResult);
        if(bindingResult.hasErrors()) {
            throw new InvalidRequestException(INVALID_REQUEST.getResponseDesc(), bindingResult);
        }

        request.setId(id);
        return segmentService.updateSegment(request);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public boolean deleteSegment(@RequestHeader("name") String name, @RequestHeader("password") String password,
                                 @PathVariable("id") Long id) {
        segmentService.deleteById(id);
        return true;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public GetSegmentsResponse getSegments(@RequestHeader("name") String name, @RequestHeader("password") String password,
                                           @RequestParam("userRef") String userRef, @RequestParam("tenantId") Long tenantId) {
        return segmentService.getSegmentsByUserId(userRef, tenantId);
    }
}
