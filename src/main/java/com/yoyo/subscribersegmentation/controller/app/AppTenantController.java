package com.yoyo.subscribersegmentation.controller.app;

import com.yoyo.subscribersegmentation.domain.response.GetSegmentsResponse;
import com.yoyo.subscribersegmentation.domain.response.TenantResponse;
import com.yoyo.subscribersegmentation.service.TenantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author anelechila
 */
@RestController
@RequestMapping("/tenants")
public class AppTenantController {

    private final TenantService tenantService;

    @Autowired
    public AppTenantController(TenantService tenantService) {
        this.tenantService = tenantService;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public TenantResponse getByCredentials(@RequestHeader("name") String name, @RequestHeader("password") String password) {
        return tenantService.getTenantByCredentials(name, password);
    }
}
