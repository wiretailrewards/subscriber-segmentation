#!/bin/sh

echo "=> Registering API with Kong ..."

json="{
    \"uris\": [
        \"/subscriber-segmentation\"
    ],
    \"name\": \"${SERVICE_NAME}\",
    \"preserve_host\": true,
    \"strip_uri\": true,
    \"upstream_url\": \"http://${KONG_DEST_HOST}/subscriber-segmentation/\"
}"

jsonactr="{
    \"uris\": [
        \"/subscriber-segmentation/actuator/\"
    ],
    \"name\": \"${SERVICE_NAME}-actuator\",
    \"preserve_host\": true,
    \"strip_uri\": true,
    \"upstream_url\": \"http://${KONG_DEST_HOST}/subscriber-segmentation/actuator/\"
}"
echo $json;
echo $jsonactr
API_RESPONSE=$(curl -X POST \
  $KONG_ENDPOINT/apis \
  -H 'content-type: application/json' \
  -d "${json}")
API_RESPONSE_ACTR=$(curl -X POST \
  $KONG_ENDPOINT/apis \
  -H 'content-type: application/json' \
  -d "${jsonactr}")
#Enabling key-auth plugin to the service
API_KEY_AUTH=$(curl -X POST --url ${KONG_ENDPOINT}/apis/${SERVICE_NAME}-actuator/plugins/ --data "name=key-auth")


#Fire all the api calls to kong
echo $API_RESPONSE
echo $API_RESPONSE_ACTR
echo $API_KEY_AUTH
