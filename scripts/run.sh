#!/bin/sh

# ===========================================================================
#  CONFIGURATION USING ENV VARIABLES. Just the ones that not checked by code.
# ===========================================================================

if [ -z ${KONG_DEST_HOST} ]
then
    echo "KONG_DEST_HOST not set so use default: internal.wigroup"
    export KONG_DEST_HOST="internal.wigroup"
fi
if [ -z ${SERVICE_NAME} ]
then
    echo "SERVICE_NAME not set "
    exit 1
fi
if [ -z ${CONSUMER_CUSTOM_ID} ]
then
    echo "CONSUMER_CUSTOM_ID not set"
fi

if [ -z ${CONSUMER_USER} ]
then
    echo "CONSUMER_USER not set "
fi

JAVA_OPTS="$JAVA_OPTS -Djava.security.egd=file:/dev/./urandom"

# =========================================================================
# Setup all apps to push to elastic search (logstash,metricbeat).
# =========================================================================
if [ -z ${ELASTICSEARCH_HOST} ]
then
    echo "ELASTICSEARCH_HOST not set. Not pushing logs to elasticsearch"
else
    sed -i "s/$ELASTICSEARCH_HOSTS/${ELASTICSEARCH_HOSTS}'#g" logstash.conf
    sed -i "s/$ELASTICSEARCH_USERNAME/${ELASTICSEARCH_USERNAME}'#g" logstash.conf
    sed -i "s/$ELASTICSEARCH_PASSWORD/${ELASTICSEARCH_PASSWORD}'#g" logstash.conf

#    Start logstash
fi

# =========================================================================
# If needed register with KONG
# =========================================================================
if [ -z ${KONG_ENDPOINT} ]
then
  echo "Variable KONG_ENDPOINT not set. Skipping Kong registration."
else
  echo "KONG_ENDPOINT set. Starting Kong registration."
  ./scripts/kong-setup.sh
fi


# TODO:  Add Elastic.co if details provided.
# =========================================================================
# Start the java app
# =========================================================================
java $JAVA_OPTS -jar $PROJECT_LOCATION/app.jar