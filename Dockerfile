FROM wigroup/aws-java:11-alpine

RUN apk add  --update \
    wget \
    sed \
    curl \
    tar

RUN mkdir -p /firebase

ENV PROJECT_LOCATION /opt/subscriber-segmentation
RUN mkdir -p ${PROJECT_LOCATION}

ARG JAR_FILE=build/libs/*.jar
COPY ${JAR_FILE} ${PROJECT_LOCATION}/app.jar

COPY scripts scripts
RUN chmod a+rx  scripts/*.sh

ENTRYPOINT ["scripts/run.sh"]